'use strict';

(function() {
	var app = angular.module('Srvc.Rss', []);

	app.service('RssSrvc', function($http) {
		return {
			getFeed: function() {
				var that = this;
				var promise = $http.jsonp('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=JSON_CALLBACK&q=http://www.vg.no/rss/feed/forsiden/?frontId=1');
				promise.then(function(result) {
					that.parseFeed(result.data.responseData);
				});
				return promise;
			},
			parseFeed: function(data) {
				
				angular.forEach(data.feed.entries, function(item) {
					item.timestamp = new Date(item.publishedDate).getTime();
					item.publishedDate = new Date(item.publishedDate);
				});
				console.log(data)
			}
		}
	});
})();