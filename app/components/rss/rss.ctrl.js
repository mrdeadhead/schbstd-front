'use strict';

(function() {
	var app = angular.module('Ctrl.Rss', []); 

	app.controller('RssCtrl', function($scope, data) {
		$scope.data = data.data.responseData.feed;
	})
})();