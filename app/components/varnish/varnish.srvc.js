'use strict';

(function() {
	var app = angular.module('Srvc.Varnish', []);

	app.service('VarnishSrvc', function($http, $q) {
		return {
			getFile: function() {
				var that = this;
				var promise = $http.get('//localhost:8080/varnish.log');
				promise.then( function(result) {
					result.data = that.getFileContents(result.data);
				});
				return promise;
			},
			getFileContents: function(data) {
				var file = data.split('\n');
				var results = { 
					hostnames: {},
					files: {}
				};
				var appendGroup = function(type, line_value) {
					results[type][line_value] = {
						'groupName': line_value.replace(/"/g,""),
						'counter': 1
					}
				}
				var sortList = function(list) {
					var sorted = [];
					angular.forEach(list, function(item) {
						sorted.push(item);
					});
					sorted.sort(function (a, b) {
						return (a['counter'] > b['counter'] ? 1 : -1);
					});

					return sorted.reverse().splice(0, 5);
				}
				for(var i=0, j=0, k=0, len = file.length-1; i<len; i++) {
					var line = file[i].split(' ');
					//grouping - should be done on DB level, this time done manually ;)
					if(i !== 0) {
						if(!results.files[line[6]]) {
							appendGroup('files', line[6])
						} else {
							results.files[line[6]].counter++;
						}

						//ignore incomplete data
						if(line[10] != '"-"') {
							if(!results.hostnames[line[10]]) {
								appendGroup('hostnames', line[10])
							} else {
								results.hostnames[line[10]].counter++;
							}
						}
					} else {
						appendGroup('files', line[6]);
						appendGroup('hostnames', line[10]);
					}
					
				}
				results.files = sortList(results.files)
				results.hostnames = sortList(results.hostnames)
				return results;
			}
		}
	});
})();