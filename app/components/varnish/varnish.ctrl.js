'use strict';

(function() {
	var app = angular.module('Ctrl.Varnish', []); 

	app.controller('VarnishCtrl', function($scope, data) {
		$scope.data = data.data;
	})
})();