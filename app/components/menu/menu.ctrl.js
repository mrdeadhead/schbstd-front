'use strict';

(function() {
	var app = angular.module('Ctrl.Menu', []); 

	app.controller('MenuCtrl', function($scope, $location) {
		$scope.routeIs = function(routeName) {
			return $location.path() === routeName;
		};
	});
})();