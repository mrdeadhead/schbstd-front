'use strict';

(function() {
	var app = angular.module('Ctrl.Jsonp', []); 

	app.controller('JsonpCtrl', function($scope, data) {
		$scope.data = data.data;
	})
})();