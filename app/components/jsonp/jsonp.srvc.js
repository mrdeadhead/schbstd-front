'use strict';

(function() {
	var app = angular.module('Srvc.Jsonp', []);

	app.service('JsonpSrvc', function($http) {
		return {
			getJsonp: function() {
				return $http({
					method: 'JSONP', 
					url: 'https://api.flickr.com/services/feeds/photos_public.gne?tags=summer&format=json',
					params: {
						'jsoncallback': 'JSON_CALLBACK'
					}
				})
			},
		}
	});
})();