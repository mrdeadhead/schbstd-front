'use strict';

(function() {
	var app = angular.module('Schibsted', ['ngRoute', 'Ctrl.Menu', 'Srvc.Varnish', 'Ctrl.Varnish', 'Srvc.Rss', 'Ctrl.Rss', 'Srvc.Jsonp', 'Ctrl.Jsonp']);

	app.config(function($routeProvider) {
		$routeProvider
			.when('/varnish', {
				templateUrl: 'app/components/varnish/varnish.html',
				controller: 'VarnishCtrl',
				menu: 'varnish',
				resolve: {
					data: function(VarnishSrvc){
						return VarnishSrvc.getFile();
					}
				}
			})
			.when('/rss', {
				templateUrl: 'app/components/rss/rss.html',
				controller: 'RssCtrl',
				menu: 'rss',
				resolve: {
					data: function(RssSrvc){
						return RssSrvc.getFeed();
					}
				}
			})
			.when('/jsonp', {
				templateUrl: 'app/components/jsonp/jsonp.html',
				controller: 'JsonpCtrl',
				menu: 'jsonp',
				resolve: {
					data: function(JsonpSrvc){
						return JsonpSrvc.getJsonp();
					}
				}
			})
			.otherwise({
				redirectTo: '/varnish'
			});
	});
})();